from pyleak.detector import Substraction, CorrCoeficient, Roi, Point
import numpy as np


def test_subtraction_detector():
    detector = Substraction(threshold=1.8)

    detector.add_roi(Roi(Point(0, 0), Point(240, 320)))
    detector.add_roi(Roi(Point(20, 20), Point(90, 80)))

    sequences = 3 * np.ones((50, 320, 240))

    detector.analyze(sequences)

    assert detector.result


def test_corr_detector():
    detector = CorrCoeficient(threshold=1.8)

    detector.add_roi(Roi(Point(0, 0), Point(240, 320)))
    detector.add_roi(Roi(Point(20, 20), Point(90, 80)))

    sequences = 3 * np.random.random((50, 320, 240))

    detector.analyze(sequences)

    assert detector.result
