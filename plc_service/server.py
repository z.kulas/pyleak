from __future__ import print_function
from twisted.internet import reactor, threads
from twisted.internet import protocol
from threading import Thread, Event
from binascii import hexlify, unhexlify
from os.path import join, realpath
import logging
import time
import os
import json

from pyleak.collector import Collector
from pyleak.analyzer import Analyzer
from pyleak.archivizer import Archivizer
from pyleak.detector import Substraction, CorrCoeficient, Roi, Point

import logger

server = logging.getLogger('pyleak_logger.server')


def read_config():
    config_path = join(os.path.dirname(realpath(__file__)), 'config.json')

    with open(config_path, 'r') as f:
        config = json.load(f)

    return config

config = read_config()
collector = Collector()  # colecting
analyzer = Analyzer()
subtraction_detector = Substraction(threshold=config.get('subtraction', {'threshold': -1.8})['threshold'])
corr_detector = CorrCoeficient(threshold=config.get('correlation', {'threshold': 0.8})['threshold'])
analyzer.add(subtraction_detector)  # analyzing
analyzer.add(corr_detector)


for roi_dict in config.get('rois', []):
    roi = Roi(Point(roi_dict['x1'], roi_dict['y1']),
              Point(roi_dict['x2'], roi_dict['y2']))
    subtraction_detector.add_roi(roi)
    corr_detector.add_roi(roi)
    server.info('Added ROI: {}'.format(roi))


class PlcCommunication(protocol.Protocol):

    def __init__(self):
        self.error = False
        self.archivizer = Archivizer()

    def dataReceived(self, data):
        server.info("Recived message from PLC {}".format(data))
        if len(data) == 4 and (hexlify(data) == '02dd0503'.encode()):
            server.info("Check connection")
            self.AckForCheckConnection()
        elif len(data) == 4 and (hexlify(data) == '02aa0503'.encode()):
            server.info("Start Analyze")
            self._check_connection()
            self._start_analyze()
        elif len(data) == 4 and (hexlify(data) == '02bb0503'.encode()):
            server.info("Stop measure")
            d = threads.deferToThread(collector.stop)
            d.addCallbacks(self.AckForStop, self.NackForStop)
        elif len(data) == 4 and (hexlify(data) == '02cc0503'.encode()):
            server.info("Get result")
            self._get_result()
        elif len(data) == 4 and (hexlify(data) == '02ff0503'.encode()):
            server.info("Stop measure wtih error")
            d = threads.deferToThread(collector.stop)
            d.addCallbacks(self.AckForErrorStop, self.NackForErrorStop)

    def AckForCheckConnection(self):
        return self.transport.write(unhexlify(b'02dd0603'))

    def AckForStart(self):
        return self.transport.write(unhexlify(b'02aa0603'))

    def NackForStart(self):
        return self.transport.write(unhexlify(b'02aa1503'))

    def AckForStop(self, msg):
        return self.transport.write(unhexlify(b'02bb0603'))

    def NackForStop(self, msg):
        return self.transport.write(unhexlify(b'02bb1503'))

    def AckForErrorStop(self, msg):
        return self.transport.write(unhexlify(b'02ff0603'))

    def NackForErrorStop(self, msg):
        return self.transport.write(unhexlify(b'02ff1503'))

    def SendResultIsOk(self):
        return self.transport.write(unhexlify(b'02cc0603'))

    def SendResultIsNOK(self):
        return self.transport.write(unhexlify(b'02cc1503'))

    def SendResultIsNotReady(self):
        return self.transport.write(unhexlify(b'02ccA103'))

    def _check_connection(self):
        try:
            collector.init()
            self.AckForStart()
        except Exception as e:
            self.NackForStart()
            server.error("Collector initialization error detailes: {}".format(e))

    def _start_analyze(self):
        measure = threads.deferToThread(collector.run)
        measure.addCallback(analyzer.run)
        measure.addCallback(self.archivizer.save)

    def _get_result(self):
        result = analyzer.get_result()
        if(result == 'NOT_READY'):
            self.SendResultIsNotReady()
        elif(result == True):
            self.SendResultIsNOK()
        elif(result == False):
            self.SendResultIsOk()

class PlcCommunicationFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return PlcCommunication()
