from twisted.internet.protocol import Protocol, ClientFactory
from sys import stdout


# class FooProtocol(Protocol):
#     def connectionMade(self):
#         # Use self.transport.write to send data to the server
#         self.transport.write('Hello server this is the Foo protocol.')
#         self.factory.do_app_logic()


# class FooFactory(ClientFactory):
#     protocol = FooProtocol

#     def __init__(self, app_object=None):
#         self.app = app_object

#     def do_app_logic(self):
#         self.app.do_something()


# class BarProtocol(Protocol):

#     def dataReceived(self, data):
#         stdout.write('Received data from server using the Bar protocol.')
#         self.factory.do_fancy_logic(data)


# class BarFactory(ClientFactory):
#     protocol = BarProtocol

#     def __init__(self, app_object=None):
#         self.app = app_object

#     def do_fancy_logic(self, data):
#         self.app.do_something_else(data)


# logic_obj = CustomAppObject()
# reactor.listenTCP(8888, FooFactory(app_object=logic_obj))
# reactor.listenTCP(9999, BarFactory(app_object=logic_obj))
# reactor.run()

# from twisted.internet import protocol, reactor


# class Echo(protocol.Protocol):
#     def dataReceived(self, data):
#         self.transport.write(data)


# class EchoFactory(protocol.Factory):
#     def buildProtocol(self, addr):
#         return Echo()


# reactor.listenTCP(8000, EchoFactory())
# reactor.run()

# from twisted.internet.deferimportDeferred
