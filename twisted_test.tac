"""The important part of this, the part that makes it a .tac file, is
the final root-level section, which sets up the object called 'application'
which twistd will look for
"""
import os
from twisted.application import service, internet
from twisted.web import static, server 

from twisted.application import internet, service
from plc_service import PlcCommunicationFactory 

application = service.Application("PlcCommunication")
PlcService = internet.TCPServer(4001, PlcCommunicationFactory())
PlcService.setServiceParent(application)