import pathlib
from setuptools import setup, find_packages

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(name='pyleak',
    version='0.1.0',
    description='The package to handle optris camera',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/z.kulas/pyleak.git',
    author='Unitem sp. z o.o.',
    author_email='zbigniew.kulas@unitem.pl',
    license='MIT',
    packages=find_packages(),
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate you support Python 3. These classifiers are *not*
        # checked by 'pip install'. See instead 'python_requires' below.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
    ],

    #package_dir={'': 'pyleak'},
    python_requires='>=3.5, <4',

    install_requires=['twisted', 'pandas', 'scipy'],

    extras_require={  # Optional
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    project_urls={
        'Bug Reports': 'https://gitlab.com/z.kulas/pyleak.git',
        'Funding': 'http://unitem.pl',
        'Source': 'https://gitlab.com/z.kulas/pyleak.git',
    },

)

