
import threading
import time
import numpy as np
import logging
#import logger

from pyoptris import camera

logger = logging.getLogger('pyleak_logger.collector')

class Collector:
    """Collector class with a stop() method. The thread has to check for the stopped() condition."""

    def __init__(self):
        self._stop_event = None
        self.frames = []
        self.camera_handler = camera.Camera()
    

    def init(self):
        self._stop_event = threading.Event()
        self.camera_handler.connect_over_tcp()
    
    def stop(self):
        self._stop_event.set()
        self.camera_handler.terminate()
    
    def run(self):
        self.frames = []
        logger.info("Start capturing frames")
        while not self._stop_event.is_set():
            self.frames.append(self.camera_handler.get_thermal_image())
            #time.sleep(0.01)
        return np.array(self.frames)                                        #not needed conversion                

        
