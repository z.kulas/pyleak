from abc import ABC, abstractmethod
import cv2
import numpy as np
import pandas as pd
import scipy.signal as signal
import scipy.stats as stat
import logging
import logger
import json

logger = logging.getLogger('pyleak_logger.detector')

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '({}, {})'.format(self.x, self.y)


class Roi:
    def __init__(self, top_left, bottom_right):
        self.top_left = top_left
        self.bottom_right = bottom_right

    def __str__(self):
        return '({}, {})'.format(self.top_left, self.bottom_right)


class Detector:
    """Base class for methods which are used for leakage detection"""

    def __init__(self, threshold):
        self.frames = None
        self.threshold = threshold
        self.rois = []

    @property
    def result(self):
        return self.result

    @property
    def frame_height(self):
        _, height, _ = self.frames.shape
        return height

    @property
    def frame_width(self):
        _, _, width = self.frames.shape
        return width

    @property
    def sequence_length(self):
        length, _, _ = self.frames.shape
        return length

    @abstractmethod
    def analyze(self):
        pass

    def mean_filter(self, frame):
        kernel = np.ones((3, 3), np.float32)/9
        return cv2.filter2D(frame, -1, kernel)

    def add_roi(self, roi):
        self.rois.append(roi)


class Substraction(Detector):
    """Class to handle patented detection method of leakage.
        Every frame is substractred from a very fist frame
        If the lowest value of subsraction every frame is lower than threshold,
        then image is marked as a leakage"""


    def __init__(self, threshold=0):
        super().__init__(threshold)
        self.min_values = np.array([])

    def analyze(self, frames):
        self.frames = frames
        x, y, z = frames.shape
        logger.info("Sequence x:{} y:{} z:{} will be analyzed".format(x, y, z))
        self.min_values = np.empty((len(self.rois) * frames.shape[0]))
        min_values_index = 0
        for idx in range(self.sequence_length):
            subtracted_frame = self.frames[idx, :, :] - self.frames[0, :, :]
            for roi in self.rois:
                self.min_values[min_values_index] = subtracted_frame[roi.top_left.y:roi.bottom_right.y,
                                                                     roi.top_left.x:roi.bottom_right.x].min()
                min_values_index += 1
        logger.info("Result of sub: MIN {} and MAX".format(self.min_values.min(), self.min_values.max()))

        return self.result

    @property
    def result(self):
        return 0 if self.min_values.min() >= self.threshold else 1


class CorrCoeficient(Detector):
    """Class to handle detection method of leakage.
        Correlation coeficient is calclulated between very first frame and every next frame
        If the lowest value of correlation is lower than threshold,
        then image is marked as a leakage"""

    def __init__(self, threshold=0):
        super().__init__(threshold)
        self.corr_coefs = np.array([])

    def analyze(self, frames):
        self.corr_coefs = np.empty((len(self.rois) * frames.shape[0]))
        self.frames = frames
        logger.info("Corr Coef was called {}".format(type(self.frames)))
        corr_coeffs_index = 0
        for frame_idx in range(self.sequence_length):
            for roi in self.rois:
                corr = np.abs(np.corrcoef(self.frames[0, roi.top_left.y:roi.bottom_right.y, roi.top_left.x:roi.bottom_right.x].flatten(),
                                          self.frames[frame_idx, roi.top_left.y:roi.bottom_right.y, roi.top_left.x:roi.bottom_right.x].flatten()))[1, 0]
                self.corr_coefs[corr_coeffs_index] = corr
                corr_coeffs_index += 1

        logger.info("Min Corr coef : {}".format(self.corr_coefs.min()))
        return self.result

    @property
    def result(self):
        return 0 if self.corr_coefs.min() >= self.threshold else 1
