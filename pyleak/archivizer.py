import os
import numpy as np
from pathlib import Path
from datetime import datetime
import logging
import logger

arch_logger = logging.getLogger('pyleak_logger.archivizer')

class Archivizer:
    """Class to archivize frames of every measurment.
    Frames will be saved as numpy array."""

    def __init__(self, dir_path='archive/'):
        self.storage_path = os.path.dirname(dir_path)

    def _get_time(self):
        return "{:%Y_%m_%d_%H_%M_%S}".format(datetime.now())

    def save(self, frames):
        measure_time = self._get_time()
        length, height, width = frames.shape
        arch_logger.debug("Save frames into {}".format(self.storage_path))
        arch_logger.debug("Sequence with size width {}, height {}, lenght {}".format(length, height, width))
        np.save(os.path.join(self.storage_path, measure_time), frames)
