import numpy as np
import logging

import logger

logger = logging.getLogger('pyleak_logger.analyzer')

class Analyzer:
    """Class dedicated to handle varoius detectors of leakage"""

    def __init__(self):
        self.detectors = [ ]
        self.is_finished = False
        self.results = []

    def add(self, detector):
        self.detectors.append(detector)

    def run(self, frames):
        self.results = []
        logger.info("Start analyzing")
        for detector in self.detectors:
            self.results.append(detector.analyze(frames))
        return frames

    def get_result(self):
        result = any(self.results) if len(self.results) == len(self.detectors) else 'NOT_READY'
        self.results = []
        return result
    
