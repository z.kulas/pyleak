import socket
from binascii import unhexlify

# Create a socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to the remote host and port
sock.connect(('localhost',  4001))

sock.send(unhexlify(b'02cc0503'))

# Get the host's response, no more than, say, 1,024 bytes
response_data = sock.recv(1024)
print(response_data)
# Terminate
sock.close()
